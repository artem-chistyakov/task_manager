FROM java:8
COPY ./build/libs/backend-0.0.1.jar /opt/backend-0.0.1.jar
WORKDIR /opt

EXPOSE 80
ENTRYPOINT ["java", "-jar", "backend-0.0.1.jar"]
CMD ["--spring.profiles.active=docker"]
