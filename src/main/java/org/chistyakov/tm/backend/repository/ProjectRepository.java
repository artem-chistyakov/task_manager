package org.chistyakov.tm.backend.repository;

import java.util.List;
import org.chistyakov.tm.backend.entity.Project;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

  Project findByIdAndUserId(String id, String userId);

  Project deleteByIdAndUserId(String id, String userId);

  List<Project> findByUserId(String userId, Pageable pageable);
}
