package org.chistyakov.tm.backend.repository;

import java.util.List;
import org.chistyakov.tm.backend.entity.Task;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

  Task deleteByIdAndUserId(String id, String userId);

  Task findByIdAndUserId(String id, String userId);

  List<Task> findByUserId(String userId, Pageable pageable);

}
