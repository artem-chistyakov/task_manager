package org.chistyakov.tm.backend.enumerated;

public enum TypeTask {
  JOB, SEX, LOVE, HEALTH, FRIENDS, STUDIES, TRANCE, ART, IMAGE, ECONOMY, FAMILY, ALTRUISM
}
