package org.chistyakov.tm.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import java.util.List;
import org.chistyakov.tm.backend.entity.Task;
import org.chistyakov.tm.backend.entity.Views;
import org.chistyakov.tm.backend.service.TaskService;
import org.jetbrains.annotations.NotNull;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/tasks")
@RestController
@CrossOrigin
public class TaskController {

  private final TaskService taskService;

  @Autowired
  public TaskController(final TaskService taskService) {
    this.taskService = taskService;
  }

  @GetMapping
  @JsonView(Views.IdName.class)
  public ResponseEntity<List<Task>> findByUserId(
      @AuthenticationPrincipal final KeycloakAuthenticationToken keycloakAuthenticationToken,
      @PageableDefault(sort = {"id"}, direction = Direction.ASC) Pageable pageable) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    final List<Task> foundTasks = taskService.findByUserId(pageable, userId);
    return ResponseEntity.ok(foundTasks);
  }


  @GetMapping("/{id}")
  @JsonView(Views.FullTask.class)
  public ResponseEntity<Task> find(
      @AuthenticationPrincipal final KeycloakAuthenticationToken keycloakAuthenticationToken,
      @PathVariable final String id) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(taskService.findById(id, userId));
  }

  @PostMapping
  public ResponseEntity<Task> create(
      @AuthenticationPrincipal @NotNull final KeycloakAuthenticationToken keycloakAuthenticationToken,
      @RequestBody final Task task) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(taskService.save(task, userId));
  }

  @PutMapping
  public ResponseEntity<Task> update(
      @AuthenticationPrincipal final KeycloakAuthenticationToken keycloakAuthenticationToken,
      @RequestBody final Task task) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(taskService.save(task, userId));
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<Task> delete(
      @AuthenticationPrincipal final KeycloakAuthenticationToken keycloakAuthenticationToken,
      @PathVariable final String id) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(taskService.deleteById(id, userId));
  }
}
