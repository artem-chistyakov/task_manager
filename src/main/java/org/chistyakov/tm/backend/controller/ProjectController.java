package org.chistyakov.tm.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import java.util.List;
import org.chistyakov.tm.backend.entity.Project;
import org.chistyakov.tm.backend.entity.Views;
import org.chistyakov.tm.backend.service.ProjectService;
import org.jetbrains.annotations.NotNull;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/projects")
@RestController
@CrossOrigin
public class ProjectController {

  private final ProjectService projectService;

  @Autowired
  public ProjectController(final ProjectService projectService) {
    this.projectService = projectService;
  }

  @GetMapping
  @JsonView(Views.IdName.class)
  public ResponseEntity<List<Project>> findByUserId(
      @AuthenticationPrincipal KeycloakAuthenticationToken keycloakAuthenticationToken,
      @PageableDefault(sort = {"id"}, direction = Direction.ASC) Pageable pageable) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    final List<Project> foundProjects = projectService.findByUserId(pageable, userId);
    return ResponseEntity.ok(foundProjects);
  }

  @GetMapping("/{id}")
  @JsonView(Views.FullProject.class)
  public ResponseEntity<Project> findByIdAndUserId(
      @AuthenticationPrincipal @NotNull KeycloakAuthenticationToken keycloakAuthenticationToken,
      @PathVariable final String id) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(projectService.findByIdAndUserId(id, userId));
  }

  @PostMapping
  public ResponseEntity<Project> create(
      @RequestBody final Project project,
      @AuthenticationPrincipal KeycloakAuthenticationToken keycloakAuthenticationToken) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(projectService.save(project, userId));
  }

  @PutMapping
  public ResponseEntity<Project> update(
      @RequestBody final Project project,
      @AuthenticationPrincipal KeycloakAuthenticationToken keycloakAuthenticationToken) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(projectService.save(project, userId));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Project> delete(
      @PathVariable final String id,
      @AuthenticationPrincipal KeycloakAuthenticationToken keycloakAuthenticationToken) {
    final String userId = keycloakAuthenticationToken.getPrincipal().toString();
    return ResponseEntity.ok(projectService.deleteByIdAndUserId(id, userId));
  }
}
