package org.chistyakov.tm.backend.service;

import java.util.List;
import org.chistyakov.tm.backend.entity.Project;
import org.chistyakov.tm.backend.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectService {

  private final ProjectRepository projectRepository;

  @Autowired
  public ProjectService(final ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  @Transactional(readOnly = true)
  public List<Project> findByUserId(
      final Pageable pageable,
      final String userId) {
    return projectRepository.findByUserId(userId, pageable);
  }

  @Transactional(readOnly = true)
  public Project findByIdAndUserId(final String id, final String userId) {
    return projectRepository.findByIdAndUserId(id, userId);
  }

  @Transactional
  public Project save(final Project project, final String userId) {
    project.setUserId(userId);
    return projectRepository.save(project);
  }

  @Transactional
  public Project deleteByIdAndUserId(final String id, final String userId) {
    return projectRepository.deleteByIdAndUserId(id, userId);
  }
}
