package org.chistyakov.tm.backend.service;

import java.time.LocalDateTime;
import java.util.List;
import org.chistyakov.tm.backend.entity.Task;
import org.chistyakov.tm.backend.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TaskService {

  private final TaskRepository taskRepository;

  @Autowired
  public TaskService(final TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  @Transactional(readOnly = true)
  public Task findById(final String id, final String userId) {
    return taskRepository.findByIdAndUserId(id, userId);
  }

  @Transactional
  public Task save(@NotNull final Task task, final String userId) {
    task.setUserId(userId);
    if (task.getDateBegin() == null) {
      task.setDateBegin(LocalDateTime.now());
    }
    if (task.isFinished() && task.getDateEnd() == null) {
      task.setDateEnd(LocalDateTime.now());
    }
    return taskRepository.save(task);
  }

  @Transactional(readOnly = true)
  public List<Task> findByUserId(
      final Pageable pageable,
      final String userId) {
    return taskRepository.findByUserId(userId, pageable);
  }

  @Transactional
  public Task deleteById(final String id, final String userId) {
    return taskRepository.deleteByIdAndUserId(id, userId);
  }
}
