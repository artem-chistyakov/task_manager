package org.chistyakov.tm.backend.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "app_projects")
@Data
@EqualsAndHashCode(of = "id")
public class Project {

  @Id
  @JsonView(Views.IdName.class)
  private String id = UUID.randomUUID().toString();

  @JsonView(Views.IdName.class)
  private String title;

  @JsonView(Views.FullProject.class)
  private String description;

  @Column(name = "date_begin", updatable = false)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-DD HH:mm:ss")
  @JsonView(Views.FullProject.class)
  private LocalDateTime dateBegin;

  @Column(name = "date_end")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-DD HH:mm:ss")
  @JsonView(Views.FullProject.class)
  private LocalDateTime dateEnd;

  @Fetch(value = FetchMode.SUBSELECT)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "project", fetch = FetchType.LAZY)
  @JsonView(Views.FullProject.class)
  private List<Task> taskList;

  @JsonView(Views.FullProject.class)
  @Column(nullable = false, name = "user_id")
  private String userId;

  @JsonView(Views.FullProject.class)
  @Column(nullable = false, name = "is_finished")
  private boolean isFinished;
}
