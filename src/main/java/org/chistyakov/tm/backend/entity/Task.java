package org.chistyakov.tm.backend.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.chistyakov.tm.backend.enumerated.TypeTask;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "app_tasks")
@Data
@EqualsAndHashCode(of = "id")
public class Task {

  @Id
  @JsonView(Views.IdName.class)
  private String id = UUID.randomUUID().toString();

  @JsonView(Views.IdName.class)
  private String title;

  @JsonView(Views.FullTask.class)
  private String description;

  @Column(name = "date_begin")
  @JsonView(Views.FullTask.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime dateBegin;

  @Column(name = "date_end")
  @JsonView(Views.FullTask.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime dateEnd;

  @Fetch(value = FetchMode.SUBSELECT)
  @Enumerated(EnumType.STRING)
  @ElementCollection(targetClass = TypeTask.class, fetch = FetchType.LAZY)
  @CollectionTable(name = "task_type", joinColumns = @JoinColumn(name = "task_id"))
  @JsonView(Views.FullTask.class)
  private Set<TypeTask> type;

  @Column(nullable = false, name = "user_id")
  @JsonView(Views.FullTask.class)
  private String userId;

  @Column(nullable = false, name = "is_finished")
  private boolean isFinished;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "project_id")
  @JsonView(Views.FullTask.class)
  private Project project;
}
