package org.chistyakov.tm.backend.entity;

public final class Views {

  public interface IdName {

  }

  public interface FullTask extends IdName {

  }

  public interface FullProject extends IdName {

  }
}
