package org.chistyakov.tm.backend.service;

import static org.junit.jupiter.api.Assertions.*;

import org.chistyakov.tm.backend.entity.Task;
import org.chistyakov.tm.backend.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class TaskServiceTest {

  @MockBean
  private TaskRepository taskRepository;

}